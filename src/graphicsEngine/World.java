package graphicsEngine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;

public abstract class World extends Pane {
	private AnimationTimer timer;
	private boolean isStopped;
	private Map<Class<?>, List<Actor>> actorsByClass;
	private Set<KeyCode> keysPressed;  
	
	
	/**
	 * Creates a world. The world will initially be a size determined by
	 * its parent Node. When start() is called, the world will begin to
	 * call act on itself and on each of its Actor children every frame.
	 */
	public World() {
		actorsByClass = new HashMap<>();
		keysPressed = new HashSet<>();
		isStopped = true;
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				act(now);
				for (Actor a : new LinkedList<Actor>(getObjects())) {
						if (a.getWorld() != null) a.act(now);
				}
			}
		};
		
		/* add a listener to the scene property such that if the world is added to a scene, it adds
		 * sets the onKeyPressed and onKeyReleased EventHandlers so that they will get the onKeyPressed/Released
		 * handler for the world and, if it is not null, call handle(event) on that handler and also get that events
		 * handler for each actor and, if it is not null, call handle(event) on that as well.
		 */
		sceneProperty().addListener(new ChangeListener<Scene>() {

			@Override
			public void changed(ObservableValue<? extends Scene> observable, Scene oldVal, Scene newVal) {
				if (newVal != null) {
					Scene scene = (Scene) newVal;
					scene.setOnKeyPressed(new EventHandler<KeyEvent>() {

						@Override
						public void handle(KeyEvent e) {
							if (getOnKeyPressed() != null) getOnKeyPressed().handle(e);
							for (Actor a : getObjects(Actor.class)) {
								if (a.getOnKeyPressed() != null) a.getOnKeyPressed().handle(e);
							}
							keysPressed.add(e.getCode());
						}
						
					});
					
					scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

						@Override
						public void handle(KeyEvent e) {
							if (getOnKeyReleased() != null) getOnKeyReleased().handle(e);
							for (Actor a : getObjects(Actor.class)) {
								if (a.getOnKeyReleased() != null) a.getOnKeyReleased().handle(e);
							}
							keysPressed.remove(e.getCode());
						}
						
					});
				}
				
			}
			
		});
	}
	
	/**
	 * Starts the timer that calls the act method on the world
	 * and on each Actor in the world each frame.
	 */
	public void start() {
		timer.start();
		isStopped = false;
	}
	
	/**
	 * Stops the timer that calls the act method.
	 */
	public void stop() {
		timer.stop();
		isStopped = true;
	}

	/**
	 * whether or not the world's timer is stopped
	 * @return true if the timer is stopped and false otherwise
	 */
	public boolean isStopped() {
		return isStopped;
	}

	/**
	 * Adds the given actor to the world at the position given by the actor's
	 * x and y coordinates.
	 * @param actor the actor to add.
	 */
	public void add(Actor actor) {
		insertActor(actor);
		addActorToActorsByClass(actor);
		actor.addedToWorld();
	}
	
	private void addActorToActorsByClass(Actor actor) {
		List<Actor> actors = actorsByClass.get(actor.getClass());
		if (actors == null) {
			actors = new LinkedList<>();
			actorsByClass.put(actor.getClass(), actors);
		}
		actors.add(actor);
		Class<?> supCls = actor.getClass().getSuperclass();
		while (Actor.class.isAssignableFrom(supCls)) {
			List<Actor> list = actorsByClass.get(supCls);
			if (list == null) {
				list = new LinkedList<>();
				actorsByClass.put(supCls, list);
			}
			list.add(actor);
			supCls = supCls.getSuperclass();
		}
	}
	
	private void removeActorFromActorsByClass(Actor actor) {
		actorsByClass.get(actor.getClass()).remove(actor);
		Class<?> supCls = actor.getClass().getSuperclass();
		while (Actor.class.isAssignableFrom(supCls)) {
			List<Actor> list = actorsByClass.get(supCls);
			if (list != null) {
				list.remove(actor);
			}
			supCls = supCls.getSuperclass();
		}
	}
	
	/**
	 * Removes the given actor from the world.
	 * @param actor The actor to remove.
	 */
	public void remove(Actor actor) {
		removeActorFromActorsByClass(actor);
		getChildren().remove(actor);
		actor.removedFromWorld(this);
	}
	
	/**
	 * Removes the actors in the given collection from the world.
	 * @param actors The collection of actors to remove.
	 */
	public void remove(Collection<? extends Actor> actors) {
		Iterator<? extends Actor> it = actors.iterator();
		while (it.hasNext()) remove(it.next());
	}
	
	/**
	 * returns a list of all the actors in the world.
	 * @return a list of all the actors in the world.
	 */
	public List<Actor> getObjects() {
		return getObjects(Actor.class);
	}
	
	/**
	 * returns a list of all the actors in the world of the given class.
	 * @param <A> the type of actors
	 * @param cls The type of actor that will be in the list
	 * @return a list of all the actors in the world of the given class.
	 */
	public <A extends Actor> List<A> getObjects(Class<A> cls) {
		List<A> list = new LinkedList<A>();
		List<Actor> actors = actorsByClass.get(cls);
		if (actors != null) {
			for (Actor a : actors) {
				list.add(cls.cast(a));
			}
		}
		return list;
	}
	
	/**
	 * Returns a list of all actors of the given class containing the given x, y
	 * @param x the x value of the contained point
	 * @param y the y values of the contained point
	 * @param cls the class of Actor that must contain the point
	 * @return a list of Actors of the given class that contain the given point
	 */
	public <A extends Actor> List<A> getObjectsAt(double x, double y, Class<A> cls) {
		ArrayList<A> list = new ArrayList<>();
		for (A a : getObjects(cls)) {
			if (a.contains(x, y)) {
				list.add(a);
			}
		}
		return list;
	}
	
	void zOrderChanged(Actor actor) {
		ObservableList<Node> children = getChildren();
		children.remove(actor);
		insertActor(actor);
	}
	
	private void insertActor(Actor actor) {
		int z = actor.getZOrder();
		ObservableList<Node> children = getChildren();
		if (children.isEmpty()) {
			children.add(actor);
		} else {
			boolean added = false;
			for (int i = children.size() - 1; i >= 0; i--) {
				Node child = children.get(i);
				if (Actor.class.isAssignableFrom(child.getClass())) {
					if (((Actor) child).getZOrder() <= z) {
						children.add(i + 1, actor);
						added = true;
						break;
					}
				}
			}
			if (!added) children.add(0, actor);
		}
	}
	
	/**
	 * Returns true if the given key is pressed and false otherwise.
	 * @param code The KeyCode for the key that is being checked
	 * @return true if the given key is pressed and false otherwise.
	 */
	public boolean isKeyPressed(KeyCode code) {
		return keysPressed.contains(code);
	}

	/**
	 * This method is called every frame once start has been called.
	 * @param now the current time in nanoseconds.
	 */
	public abstract void act(long now);
	
}
