package graphicsEngine;

import javafx.scene.media.AudioClip;

public class Sound {
	private final AudioClip player;
	
	public Sound(String fileName) {
		player = new AudioClip(getClass().getClassLoader().getResource(fileName).toString());
	}

	public AudioClip getPlayer() {
		return player;
	}
	
	public void play() {
		player.play();
	}
	
	public void stop() {
		player.stop();
	}
	
	public void setRepeat(boolean repeat) {
		if (repeat) {
			player.setCycleCount(AudioClip.INDEFINITE);
		} else {
			player.setCycleCount(1);
		}
	}
	
}
