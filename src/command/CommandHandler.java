package command;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public abstract class CommandHandler {
	private static final boolean DEBUG = false;
	
	protected static <C extends CommandHandler> HashMap<String, C> initCommandMap(Class<C> topClass) {
		HashMap<String, C> map = new HashMap<>();
		File dir = null;
		List<Class<?>> classes = null;
		File rootDir = new File(".");
		File[] files = rootDir.listFiles();
		for (File f : files) {
			// this means we are in the ide not running from a jar file
			//(could fail if there happens to be a copy of the
			// jar in the directory where jars are being run
			if (f.getName().equals("multiplayerGameEngine.jar")) {
				dir = new File(".");
				break;
			}
		}
		if (dir == null) {
			URL url = CommandHandler.class.getProtectionDomain().getCodeSource().getLocation();
			classes = getClassesFromJar(url);
		} else {
			classes = findClasses(dir, null);
			classes.addAll(getClassesFromJars(dir));
		}
		debugMsg(classes.toString());
		for (Class<?> cls : classes) {
			if (topClass.isAssignableFrom(cls)) {
				try {
					// Since we just checked if topClass is assignable from cls and C is topClass the type cast is safe
					@SuppressWarnings("unchecked")
					C cmd = (C)cls.newInstance();
					map.put(cmd.getCommandWord(), cmd);
				} catch (InstantiationException | IllegalAccessException e) {
					if (DEBUG) e.printStackTrace();
				}
			}
		}
		return map;
	}
	
	static void debugMsg(String msg) {
		if (DEBUG) {
			System.out.println(msg);
		}
	}
	
	private static List<Class<?>> findClasses(File directory, List<String> ignoredFiles) {
	    List<Class<?>> classes = new ArrayList<Class<?>>();
	    if (!directory.exists()) {
	        return classes;
	    }
	    File[] files = directory.listFiles();
	    
	    for (File file : files) {
	    	if (file.isDirectory()) {
	    		classes.addAll(findClasses(file, ignoredFiles));
	    	} else if (file.getName().endsWith(".class")) {
				if (ignoredFiles == null || !ignoredFiles.contains(file.getName())) {
					List<String> pathComponents = filePathComponents(file);
					debugMsg("Starting Path Components = " + pathComponents);
					if (pathComponents.size() > 0 && pathComponents.get(0).equals(".")) {
						debugMsg("Removing starting . from path components");
						pathComponents.remove(0);
						debugMsg("Path Components now = " + pathComponents);
					}
					while (pathComponents.size() > 0) {
						debugMsg("Current Path Components = " + pathComponents);
						String fullClassName = "";
						for (int i = 0; i < pathComponents.size() - 1; i++) {
							fullClassName += pathComponents.get(i) + ".";
						}
						// last component will be the file name
						fullClassName += file.getName().substring(0, file.getName().length() - 6);
						Class<?> cls = null;
						
						try {
							debugMsg("attempting to get instance of class: " + fullClassName);
							cls = Class.forName(fullClassName);
							classes.add(cls);
							debugMsg("Successfully created instance of class " + fullClassName);
							break;
						} catch (ClassNotFoundException e) {
							debugMsg("Class Not Found: " + fullClassName);
							pathComponents.remove(0);
						}
					}
				} 
	    	}
	    }
	    return classes;
	}
	
	public static List<Class<?>> getClassesFromJar(URL url) {
		List<Class<?>> list = new ArrayList<>();
		try (JarInputStream jar = new JarInputStream(url.openStream());) {
			while (true) {
				JarEntry entry = jar.getNextJarEntry();
				if (entry == null) {
					break;
				} else if (entry.getName().endsWith(".class")) {
					String fName = entry.getName();
					try {
						Class<?> cls = Class.forName(entry.getName().substring(0, fName.length() - 6).replaceAll("/", "."));
						list.add(cls);
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException er) {
			er.printStackTrace();
		}
		return list;
	}
	
	public static List<Class<?>> getClassesFromJars(File root){
		List<Class<?>> list = new ArrayList<>();
		for (File f : root.listFiles()) {
			if (f.getName().endsWith(".jar")) {
				URL url;
				try {
					url = f.toURI().toURL();
					list.addAll(getClassesFromJar(url));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	private static List<String> filePathComponents(File f) {
		List<String> components = new ArrayList<String>();
	    do {
	    	components.add(f.getName());
	        f = f.getParentFile();
	    } while (f.getParentFile() != null);
	    components.add(f.getName());
	    Collections.reverse(components);
	    return components;
	}
	
	public static String getCommandString(CommandHandler cmd, String[] params) {
		String str = cmd.getCommandWord();
		for (String s : params) {
			str += " " + s;
		}
		return str;
	}
	
	public static String[] getCommandParameters(String cmdStr) {
		String[] words = cmdStr.split("\\s+");
		if (words.length > 1) return Arrays.copyOfRange(cmdStr.split("\\s+"), 1, words.length);
		return new String[0];
	}
	
	/**
	 * Returns the command word substring of the given string (i.e. the first word).
	 * @param cmdStr The full command string (should start with a command word)
	 * @return the command word substring of the given string
	 */
	public static String getCommandWord(String cmdStr) {
		if (cmdStr == null || cmdStr.length() == 0) return null;
		return cmdStr.split("\\s+")[0];
	}
	
	/**
	 * Returns the command word string that is associated with this command.
	 * @return The command word that is associated with this command.
	 */
	public abstract String getCommandWord();
	
}
