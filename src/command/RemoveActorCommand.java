package command;

import gameClient.MultiplayerActor;
import gameClient.MultiplayerWorld;

public class RemoveActorCommand extends ClientCommandHandler {

	@Override
	public void doCommand(String cmd, String[] params, MultiplayerWorld world) {
		if (params.length < 1) throw new IllegalArgumentException("Not enough parameters. Correct params are <id>");
		String id = params[0];
		MultiplayerActor actor = world.actorWithId(id);
    	if (actor != null) world.remove(actor);
	}

	@Override
	public String getCommandWord() {
		return "RA";
	}

	public String getRemoveActorCmdStr(String id) {
		return getCommandString(this, new String[]{id});
	}
}
