

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import gameServer.CollisionMap;

public class CollisionMapTester {

	public static void main(String[] args) {
		InputStream stream = CollisionMap.class.getClassLoader().getResourceAsStream("playerShip2_red.png");
		try (	FileWriter fwOrig = new FileWriter(new File("original.txt"));
				FileWriter fwRot = new FileWriter(new File("rotated.txt"));
				FileWriter fwRotScaled = new FileWriter(new File("rotated+scaled.txt"));){
			BufferedImage img = ImageIO.read(stream);
			CollisionMap map = new CollisionMap(img);
			fwOrig.write(map.toString());
			CollisionMap rot = map.getRotatedMap(Math.toRadians(45));//new CollisionMap(map, Math.toRadians(45));
			fwRot.write(rot.toString());
			CollisionMap scaled = rot.getScaledMap(0.3, 0.3);
			fwRotScaled.write(scaled.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
