package gameServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import command.ServerCommandHandler;

public class ClientThread implements Runnable {
	private Socket csocket;
	private MultiThreadServer server;
	private String id;

	public ClientThread(Socket csocket, MultiThreadServer server, String id) {
		this.csocket = csocket;
		this.server = server;
		this.id = id;
	}
	
	private String appendIdToInput(String input) {
		return input + " " + id;
	}

	public void run() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
			String inputLine = in.readLine();
			while (inputLine != null) {
				final String cmd = inputLine;
				server.getGameWorld().runTask(()-> {
					ServerCommandHandler.doCommand(appendIdToInput(cmd), server.getGameWorld());
				});
				inputLine = in.readLine();
			}
			csocket.close();
			server.getActiveClients().remove(id);
			ServerWorld w = server.getGameWorld();
			w.runTask(()->{
				w.onClientDisconnect(id);
			});
		} catch (IOException e) {
			server.getActiveClients().remove(id);
			ServerWorld w = server.getGameWorld();
			w.runTask(()->{
				w.onClientDisconnect(id);
			});
			System.out.println(e);
		}
	}
}