package gameServer;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public abstract class ActorModel {
	
	private double x;
	private double y;
	private int zOrder;
	private double centerX;
	private double centerY;
	private double scaleX;
	private double scaleY;
	private double rotation;
	private WorldModel world;
	private CollisionMap collisionMap;
	private CollisionMap transformedCollisionMap;
	
	public ActorModel() {
		this(new CollisionMap((BufferedImage)null));
	}
	
	public ActorModel(InputStream inputStream) throws IOException {
		this(new CollisionMap(inputStream));
	}
	
	public ActorModel(BufferedImage img) {
		this(new CollisionMap(img));
	}
	
	public ActorModel(CollisionMap map) {
		this.collisionMap = map;
		this.x = 0;
		this.y = 0;
		this.zOrder = 0;
		this.centerX = 0;
		this.centerY = 0;
		this.scaleX = 1.0;
		this.scaleY = 1.0;
		this.rotation = 0;
		transformedCollisionMap = null;
	}
	
	public void setZOrder(int z) {
		zOrder = z;
	}
	
	public int getZOrder() {
		return zOrder;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.centerX += x - this.x;
		this.x = x;
		
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.centerY += y - this.y;
		this.y = y;
	}

	public double getCenterX() {
		return centerX;
	}

	public double getCenterY() {
		return centerY;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
		transformedCollisionMap = null;
	}
	
	private CollisionMap getCollisionMap() {
		if (transformedCollisionMap == null) {
			transformedCollisionMap = transformedCollisionMap();
		}
		return transformedCollisionMap;
	}

	private CollisionMap transformedCollisionMap() {
		AffineTransform trans = new AffineTransform();
		trans.rotate(getRotation(), collisionMap.getWidth() / 2, collisionMap.getWidth() / 2);
		trans.scale(getScaleX(), getScaleY());
		return collisionMap.getTransformedMap(trans);
	}
	
	public void setImage(BufferedImage img) {
		collisionMap = new CollisionMap(img);
		this.centerX = getX() + img.getWidth() / 2;
		this.centerY = getY() + img.getHeight() / 2;
		transformedCollisionMap = null;
	}

	public double getScaleX() {
		return scaleX;
	}

	public double getScaleY() {
		return scaleY;
	}

	public void setScale(double scaleX, double scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
		transformedCollisionMap = null;
	}

	/**
	 * Moves this actor by the given dx and dy.
	 * @param dx the amount to move horizontally (change in x)
	 * @param dy the amount to move vertically (change in y)
	 */
	public void move(double dx, double dy) {
		setX(getX() + dx);
		setY(getY() + dy);
	}
	
	/**
	 * returns the world this actor is in, or null if it is not in a world.
	 * @return the world this actor is in, or null if it is not in a world
	 */
	public WorldModel getWorld() {
		return world;
	}
	
	void setWorld(WorldModel world) {
		this.world = world;
	}
	
	/**
	 * Returns The width of the current image of this actor.
	 * @return the width of the current image of this actor, taking into account any transformations.
	 */
	public double getWidth() {
		return getCollisionMap().getWidth();
	}
	
	/**
	 * Returns The height of the current image of this actor.
	 * @return the height of the current image of this actor, taking into account any transformations.
	 */
	public double getHeight() {
		return getCollisionMap().getHeight();
	}
	
	/**
	 * Returns a list of all the actors intersecting this actor.
	 * @return a list of all the actors intersecting this actor
	 */
	public List<ActorModel> getIntersectingObjects() {
		return getIntersectingActors(ActorModel.class);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)getX(), (int)getY(), (int)getWidth(), (int)getHeight());
	}
	
	public boolean intersects(ActorModel actor) {
		return getCollisionMap().intersects(new Point((int)getX(), (int)getY()),
				actor.getCollisionMap(), new Point((int)actor.getX(), (int)actor.getY()));
	}
	
	public boolean contains(double x, double y) {
		return getBounds().contains(x, y);
	}
	
	/**
	 * Returns a list of the actors of a given type intersecting this actor.
	 * @param <A> the class of intersecting actors that will be in the returned list
	 * @param cls The type of intersecting actors that should be in the list
	 * @return a list of all actors of the given type intersecting this actor
	 */
	public <A extends ActorModel> List<A> getIntersectingActors(Class<A> cls) {
		ArrayList<A> list = new ArrayList<>();
		for (A a : getWorld().getActors(cls)) {
			if (a != this && intersects(a)) list.add(a);
		}
		return list;
	}
	
	/**
	 * Returns one actor of the given class that is intersecting this actor.
	 * @param <A> the class of intersecting actor that will be in the returned
	 * @param cls the type of actor to return
	 * @return an intersecting actor of the given class, or null if no such actor
	 */
	public <A extends ActorModel> A getOneIntersectingActor(Class<A> cls) {
		for (A a : getWorld().getActors(cls)) {
			if (a != this && intersects(a)) return a;
		}
		return null;
	}
	
	/**
	 * This method is called every frame once start has been called on the world.
	 */
	public abstract void act();
	
	/**
	 * This method is called when an actor is added to the world using the
	 * add method of a world.
	 */
	public void addedToWorld() {
		// meant to be overridden.
	}
	
	public void removedFromWorld(WorldModel world) {
		// meant to be overriden
	}
}
