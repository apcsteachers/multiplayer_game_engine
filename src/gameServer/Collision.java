package gameServer;
import javafx.geometry.Point2D;

public class Collision {
	private final Point2D resultV1;
	private final Point2D resultV2;
	
	public Collision(double m1, Point2D v1, Point2D p1, double m2, Point2D v2, Point2D p2) {
		
		// normal vector
		Point2D n = new Point2D(p2.getX() - p1.getX(), p2.getY() - p1.getY());
		
		// normal vector (between centers)
		Point2D un = n.normalize();
		
		// unit tangent vector
		Point2D ut = new Point2D(-un.getY(), un.getX());
		
		double v1n = un.dotProduct(v1);
		double v1t = ut.dotProduct(v1);
		double v2n = un.dotProduct(v2);
		double v2t = ut.dotProduct(v2);
		
		double v1tPrime =v1t;
		double v2tPrime = v2t;
		
		double v1nPrime = (v1n * (m1 - m2) + 2 * m2 * v2n) / (m1 + m2);
		double v2nPrime = (v2n * (m2 - m1) + 2 * m1 * v1n) / (m1 + m2);
		
		Point2D v1nPrimeVector = un.multiply(v1nPrime);
		Point2D v1tPrimeVector = ut.multiply(v1tPrime);
		Point2D v2nPrimeVector = un.multiply(v2nPrime);
		Point2D v2tPrimeVector = ut.multiply(v2tPrime);
		
		resultV1 = v1nPrimeVector.add(v1tPrimeVector);
		resultV2 = v2nPrimeVector.add(v2tPrimeVector);
	}

	public Point2D getResultV1() {
		return resultV1;
	}

	public Point2D getResultV2() {
		return resultV2;
	}
	
}
