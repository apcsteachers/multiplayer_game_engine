package gameServer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class WorldModel {
	private ScheduledThreadPoolExecutor timer;
	private boolean isStopped;
	private Map<Class<?>, List<ActorModel>> actorsByClass;
	private List<ActorModel> actors;
	private double width;
	private double height;
	private ConcurrentLinkedQueue<Runnable> queue;
	
	private final long DELAY; // determines framerate
	
	
	/**
	 * Creates a world of the given width and height. When start() is called, the world will begin to
	 * call act on itself and on each of its Actor children every frame with the given delay between frames.
	 */
	public WorldModel(double width, double height, long delay) {
		this.width = width;
		this.height = height;
		DELAY = delay;
		actorsByClass = new HashMap<>();
		actors = new ArrayList<>();
		queue = new ConcurrentLinkedQueue<>();
		isStopped = true;
		timer = new ScheduledThreadPoolExecutor(1);
		timer.scheduleAtFixedRate(()->{
			try {
				if (!isStopped) {
					runQueuedTasks();
					onTick();
				}
			} catch(Throwable er) {
				er.printStackTrace();
			}
		}, DELAY, DELAY, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Does what needs to be done each frame. Calls act on the world and on each actor in the world.
	 */
	protected void onTick() {
		act();
		for (ActorModel a : new LinkedList<ActorModel>(actors)) {
				if (a.getWorld() != null) a.act();
		}
	}
	
	/**
	 * Starts the timer that calls the act method on the world
	 * and on each Actor in the world each frame.
	 */
	public void start() {
		isStopped = false;
	}
	
	/**
	 * Stops the timer that calls the act method.
	 */
	public void stop() {
		isStopped = true;
	}

	/**
	 * whether or not the world's timer is stopped
	 * @return true if the timer is stopped and false otherwise
	 */
	public boolean isStopped() {
		return isStopped;
	}

	/**
	 * Adds the given actor to the world at the position given by the actor's
	 * x and y coordinates.
	 * @param actor the actor to add.
	 */
	public void add(ActorModel actor) {
		actors.add(actor);
		addActorToActorsByClass(actor);
		actor.setWorld(this);
		actor.addedToWorld();
	}
	
	private void addActorToActorsByClass(ActorModel actor) {
		List<ActorModel> actors = actorsByClass.get(actor.getClass());
		if (actors == null) {
			actors = new LinkedList<>();
			actorsByClass.put(actor.getClass(), actors);
		}
		actors.add(actor);
		Class<?> supCls = actor.getClass().getSuperclass();
		while (ActorModel.class.isAssignableFrom(supCls)) {
			List<ActorModel> list = actorsByClass.get(supCls);
			if (list == null) {
				list = new LinkedList<>();
				actorsByClass.put(supCls, list);
			}
			list.add(actor);
			supCls = supCls.getSuperclass();
		}
	}
	
	private void removeActorFromActorsByClass(ActorModel actor) {
		actorsByClass.get(actor.getClass()).remove(actor);
		Class<?> supCls = actor.getClass().getSuperclass();
		while (ActorModel.class.isAssignableFrom(supCls)) {
			List<ActorModel> list = actorsByClass.get(supCls);
			if (list != null) {
				list.remove(actor);
			}
			supCls = supCls.getSuperclass();
		}
	}
	
	/**
	 * Removes the given actor from the world.
	 * @param actor The actor to remove.
	 */
	public void remove(ActorModel actor) {
		removeActorFromActorsByClass(actor);
		actors.remove(actor);
		actor.setWorld(null);
		actor.removedFromWorld(this);
	}
	
	/**
	 * Removes the actors in the given collection from the world.
	 * @param actors The collection of actors to remove.
	 */
	public void remove(Collection<? extends ActorModel> actors) {
		Iterator<? extends ActorModel> it = actors.iterator();
		while (it.hasNext()) remove(it.next());
	}
	
	/**
	 * returns a list of all the actors in the world.
	 * @return a list of all the actors in the world.
	 */
	public List<ActorModel> getActors() {
		return getActors(ActorModel.class);
	}
	
	/**
	 * returns a list of all the actors in the world of the given class.
	 * @param <A> the type of actors
	 * @param cls The type of actor that will be in the list
	 * @return a list of all the actors in the world of the given class.
	 */
	public <A extends ActorModel> List<A> getActors(Class<A> cls) {
		List<A> list = new LinkedList<A>();
		List<ActorModel> actors = actorsByClass.get(cls);
		if (actors != null) {
			for (ActorModel a : actors) {
				list.add(cls.cast(a));
			}
		}
		return list;
	}
	
	/**
	 * Returns a list of all actors of the given class containing the given x, y
	 * @param x the x value of the contained point
	 * @param y the y values of the contained point
	 * @param cls the class of Actor that must contain the point
	 * @return a list of Actors of the given class that contain the given point
	 */
	public <A extends ActorModel> List<A> getActorsAt(double x, double y, Class<A> cls) {
		ArrayList<A> list = new ArrayList<>();
		for (A a : getActors(cls)) {
			if (a.contains(x, y)) {
				list.add(a);
			}
		}
		return list;
	}

	/**
	 * Returns the width of the world
	 * @return the width of the world
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * Sets the width of the world
	 * @param width the new width
	 */
	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Returns the height of the world
	 * @return the height of the world
	 */
	public double getHeight() {
		return height;
	}

	/**
	 * Sets the height of the world
	 * @param height the new height
	 */
	public void setHeight(double height) {
		this.height = height;
	}
	
	public void runTask(Runnable task) {
		queue.add(task);
	}
	
	private void runQueuedTasks() {
		Runnable task = queue.poll();
		while (task != null) {
			task.run();
			task = queue.poll();
		}
	}

	/**
	 * This method is called every frame once start has been called.
	 */
	public abstract void act();
}
