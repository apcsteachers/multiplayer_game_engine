package gameServer;

import command.SetRotationCommand;
import command.SetScaleCommand;
import command.SetXCommand;
import command.SetYCommand;
import command.SetZOrderCommand;
import gameClient.MultiplayerActor;

public abstract class MultiplayerActorModel extends ActorModel {
	
	// ID that uniquely identifies this actor so actors on both client and server side can be correlated
	private String actorId;
	
	// class corresponding to this class on the client side
	private Class<?> clientClass;
	
	
	/**
	 * Constructs an actor with the given id
	 * @param actorId the unique id assigned to this actor
	 */
	public MultiplayerActorModel(String actorId) {
		this.actorId = actorId;
		clientClass = null;
	}

	/**
	 * Returns the actorId
	 * @return the actorId
	 */
	public String getActorId() {
		return actorId;
	}
	
	@Override
	public void setZOrder(int z) {
		if (getZOrder() != z) {
			super.setZOrder(z);
			broadcast(new SetZOrderCommand().getSetZOrderCmdStr(getActorId(), getZOrder()));
		}
	}

	@Override
	public void setX(double x) {
		if (getX() != x) {
			super.setX(x);
			broadcast(new SetXCommand().getSetXCmdStr(getActorId(), getX()));
		}
	}

	@Override
	public void setY(double y) {
		if (getY() != y) {
			super.setY(y);
			broadcast(new SetYCommand().getSetYCmdStr(getActorId(), getY()));
		}
	}
	
	@Override
	public void setRotation(double rotation) {
		if (getRotation() != rotation) {
			super.setRotation(rotation);
			broadcast(new SetRotationCommand().getSetRotationCmdStr(getActorId(), getRotation()));
		}
		
	}

	@Override
	public void setScale(double scaleX, double scaleY) {
		if (getScaleX() != scaleX || getScaleY() != scaleY) {
			super.setScale(scaleX, scaleY);
			broadcast(new SetScaleCommand().getSetScaleCmdStr(getActorId(), getScaleX(), getScaleY()));
		}
	}

	public void broadcast(String message) {
		ServerWorld world = (ServerWorld)getWorld();
		if (world != null && world.getServer() != null) {
			world.getServer().broadcast(message);
		}
	}

	public Class<?> getClientClass() {
		return clientClass;
	}
	
	protected void setClientClass(Class<? extends MultiplayerActor> cls) {
		clientClass = cls;
	}
}
