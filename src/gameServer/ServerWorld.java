package gameServer;
import java.util.concurrent.ConcurrentHashMap;

import command.AddActorCommand;
import command.RemoveActorCommand;

public abstract class ServerWorld extends WorldModel {
	private ConcurrentHashMap<String, MultiplayerActorModel> multiplayerActors;
	private MultiThreadServer server;
	
	/**
	 * Create a new width x height ServerWorld with a reference to the given server.
	 * @param width the world width
	 * @param height the world height
	 * @param server the server
	 */
	public ServerWorld(double width, double height, MultiThreadServer server) {
		this(width, height, 33, server);
	}
	
	/**
	 * Create a new width x height ServerWorld with a reference to the given server and the given delay between frames.
	 * @param width width of the world
	 * @param height height of the world
	 * @param delay delay between frames (higher delay means less frames per second)
	 * @param server the server
	 */
	public ServerWorld(double width, double height, long delay, MultiThreadServer server) {
		super(width, height, delay);
		multiplayerActors = new ConcurrentHashMap<>();
		this.server = server;
		server.setWorld(this);
	}

	public void remove(MultiplayerActorModel actor) {
		if (actor != null) {
			String id = actor.getActorId();
			multiplayerActors.remove(id);
			super.remove(actor);
			server.broadcast(new RemoveActorCommand().getRemoveActorCmdStr(id));
		}
	}
	
	@Override
	public void remove(ActorModel actor) {
		if (actor instanceof MultiplayerActorModel) remove((MultiplayerActorModel)actor);
		else super.remove(actor);
	}

	public void removeActor(String id) {
		MultiplayerActorModel p = multiplayerActors.get(id);
		remove(p);
	}
	
	public void add(MultiplayerActorModel actor) {
		super.add(actor);
		String id = actor.getActorId();
		multiplayerActors.put(id, actor);
		server.broadcast(new AddActorCommand().getAddActorCmdStr(id, actor.getClientClass(), actor.getX(), actor.getY(), actor.getScaleX(), actor.getScaleY(), actor.getRotation(), actor.getZOrder()));
	}
	
	@Override
	public void add(ActorModel actor) {
		if (actor instanceof MultiplayerActorModel) add((MultiplayerActorModel)actor); 
		else super.add(actor);
	}

	public ConcurrentHashMap<String, MultiplayerActorModel> getMultiplayerActors() {
		return multiplayerActors;
	}

	public void setMultiplayerActors(ConcurrentHashMap<String, MultiplayerActorModel> multiplayerActors) {
		this.multiplayerActors = multiplayerActors;
	}

	public void setServer(MultiThreadServer server) {
		this.server = server;
	}

	public MultiThreadServer getServer() {
		return server;
	}

	public abstract void onClientConnect(String id);
	public abstract void onClientDisconnect(String id);
}
