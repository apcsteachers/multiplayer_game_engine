package gameServer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class CollisionMap {
	private final char TRUE_CHAR = '*';
	private final char FALSE_CHAR = '_';
	
	private BufferedImage img;
	private int offX;
	private int offY;
	
	public CollisionMap(CollisionMap map) {
		this(map.img);
	}
	
	public CollisionMap(BufferedImage img) {
		this.img = img;
		offX = 0;
		offY = 0;
	}
	
	public CollisionMap(BufferedImage img, int offX, int offY) {
		this.img = img;
		this.offX = offX;
		this.offY = offY;
	}
	
	public CollisionMap(InputStream fileStream) throws IOException {
		this(ImageIO.read(fileStream));
	}
	
	public CollisionMap getTransformedMap(AffineTransform transform) {
		if (img == null) return new CollisionMap((BufferedImage)null);
		AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		return new CollisionMap(op.filter(img, null));
	}
	
	public CollisionMap getTrimmedMap() {
		
		BufferedImage bi = img;
		
		int centerX = img.getWidth() / 2;
		int centerY = img.getHeight() / 2;
		
		int minRow = minRow(bi);
		int maxRow = maxRow(bi);
		int minCol = minCol(bi);
		int maxCol = maxCol(bi);
		
		int newW = maxCol - minCol + 1;
		int newH = maxRow - minRow + 1;
		BufferedImage trimmedImg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = trimmedImg.createGraphics();
		g.drawImage(bi.getSubimage(minCol, minRow, newW, newH), 0, 0, null);
		g.dispose();
		int newCenterX = minCol + trimmedImg.getWidth() / 2;
		int newCenterY = minRow + trimmedImg.getHeight() / 2;
		int centerOffX = newCenterX - centerX;
		int centerOffY = newCenterY - centerY;
		return new CollisionMap(trimmedImg, centerOffX, centerOffY);
	}
	
	public int minRow(BufferedImage img) {
		int h = img.getHeight();
		int w = img.getWidth();
		for (int r = 0; r < h; r++) {
			for (int c = 0; c < w; c++) {
				if (new Color(img.getRGB(c, r), true).getAlpha() > 0) {
					return r;
				}
			}
		}
		return h - 1;
	}
	
	public int maxRow(BufferedImage img) {
		int h = img.getHeight();
		int w = img.getWidth();
		for (int r = h - 1; r >= 0; r--) {
			for (int c = 0; c < w; c++) {
				if (new Color(img.getRGB(c, r), true).getAlpha() > 0) {
					return r;
				}
			}
		}
		return 0;
	}
	
	public int minCol(BufferedImage img) {
		int h = img.getHeight();
		int w = img.getWidth();
		for (int c = 0; c < w; c++) {
			for (int r = 0; r < h; r++) {
				if (new Color(img.getRGB(c, r), true).getAlpha() > 0) {
					return c;
				}
			}
		}
		return w - 1;
	}
	
	public int maxCol(BufferedImage img) {
		int h = img.getHeight();
		int w = img.getWidth();
		for (int c = w - 1; c >= 0; c--) {
			for (int r = 0; r < h; r++) {
				if (new Color(img.getRGB(c, r), true).getAlpha() > 0) {
					return c;
				}
			}
		}
		return 0;
	}
	
	public CollisionMap getRotatedMap(double rotation) {
		if (img == null) return new CollisionMap((BufferedImage)null);
		AffineTransform trans = new AffineTransform();
	    trans.rotate(rotation, img.getWidth()/2, img.getHeight()/2);
		return getTransformedMap(trans);
	}
	
	public CollisionMap getScaledMap(double scaleX, double scaleY) {
		if (img == null) return new CollisionMap((BufferedImage)null);
		scaleX = Math.max(scaleX, 0);
		scaleY = Math.max(scaleY, 0);
		AffineTransform trans = new AffineTransform();
		trans.scale(scaleX, scaleY);
		return getTransformedMap(trans);
	}
	
	public int getWidth() {
		return img == null ? 0 : img.getWidth();
	}
	
	public int getHeight() {
		return img == null ? 0 : img.getHeight();
	}
	
	public boolean intersects(Point pos, CollisionMap other, Point otherPos) {
		if (img == null) return false;
		Rectangle rect = new Rectangle(pos.x, pos.y, getWidth(), getHeight());
		Rectangle rectOther = new Rectangle(otherPos.x, otherPos.y, other.getWidth(), other.getHeight());
		if (!rect.intersects(rectOther)) return false;
		else {
			Rectangle intersection = rect.intersection(rectOther);
			for (int iRow = 0; iRow < intersection.getHeight(); iRow++) {
				for (int iCol = 0; iCol < intersection.getWidth(); iCol++) {
					int iX = iCol + intersection.x;
					int iY = iRow + intersection.y;
					int col = iX - pos.x;
					int row = iY - pos.y;
					
					boolean collideable = isNonTransparentPixel(row, col);
					if (collideable) {
						int otherCol = iX - otherPos.x;
						int otherRow = iY - otherPos.y;
						if (other.isNonTransparentPixel(otherRow, otherCol)) return true;
					}
				}
			}
			return false;
		}
	}
	
	public BufferedImage getImage() {
		return img;
	}
	
	public boolean isNonTransparentPixel(int r, int c) {
		return img == null ? false : new Color(img.getRGB(c, r), true).getAlpha() > 0;
	}
	
	
	
	public int getOffX() {
		return offX;
	}

	public int getOffY() {
		return offY;
	}

	public void saveToFile(File file) throws IOException {
		FileWriter fw = new FileWriter(file);
		fw.write(this.toString());
		fw.close();
	}
	
	@Override
	public String toString() { 
		String s = "";
		if (img == null) return s;
		for (int r = 0; r < img.getHeight(); r++) {
			for (int c = 0; c < img.getWidth(); c++) {
				s += (isNonTransparentPixel(r, c) ? TRUE_CHAR : FALSE_CHAR) + " ";
			}
			s += "\r\n";
		}
		return s;
	}
	
}
